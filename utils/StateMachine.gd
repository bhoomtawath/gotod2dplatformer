extends Node

var state

func transition_to(new_state_path):
	var new_state = get_node(new_state_path)
	if new_state != null:
		state = new_state
		state.on_entered()
		
func physics_process(delta):
	if state != null:
		state.physics_process(delta)
		
