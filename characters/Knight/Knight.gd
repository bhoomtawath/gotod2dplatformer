extends KinematicBody2D

signal hitted

enum States {
	IDLE,
	DYING,
	DEATH
}

export var health = 100
export var gravity = Vector2(0, 500.0)
onready var animation = $AnimatedSprite
onready var staggered_timer = $StaggeredTimer
onready var death_timer = $DeathTImer
onready var hurt_box = $HurtBox
onready var health_bar = $UI/HealthBar
var velocity = Vector2.ZERO

export var state = States.IDLE

func _ready():
	animation.play("idle")
	staggered_timer.connect("timeout", self, "_on_StaggeredTimer_timeout")
	death_timer.connect("timeout", self, "_on_DeathTimer_timeout")
	health_bar.set_value(health)

func _physics_process(delta):
	var new_velocity = velocity + gravity * delta
	velocity = move_and_slide(new_velocity, Vector2.UP)

func on_hitted(damage):
	if state == States.IDLE:
		health -= damage
		health = clamp(health, 0, damage)
		animation.play("staggered")
		staggered_timer.start()
		emit_signal("hitted")
		health_bar.set_value(health)
		if health <= 0:
			_death()

func _on_StaggeredTimer_timeout():
	if state == States.IDLE:
		animation.play("idle")
		staggered_timer.stop()

func _death():
	collision_layer = 2
	animation.play("dying")
	state = States.DYING
	death_timer.start()

func _on_DeathTimer_timeout():
	death_timer.stop()
	animation.play("idle")
	state = States.IDLE
	health = 100
	health_bar.set_value(health)
	collision_layer = 1

func _on_AnimatedSprite_animation_finished():
	if state == States.DYING:
		animation.play("death")
	elif state == States.DEATH:
		animation.stop()
		
func _on_Player_moved(player_position):
	var relative_position = player_position.x - global_position.x
	if relative_position < 0:
		animation.flip_h = true
	else:
		animation.flip_h = false
