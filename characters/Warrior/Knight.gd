extends KinematicBody2D

signal attack_hitted(damage, body)
signal moved(position)

export var jump_speed = 600.0

var floor_vector = Vector2(0, -1)

onready var animation = $AnimationPlayer
onready var sprite = $AnimatedSprite
onready var state_machine = $StateMachine
onready var player = $Player

func _ready():
	transition_state("Move/Idle")

func transition_state(new_state_path):
	state_machine.transition_to(new_state_path)
	
func _physics_process(delta):
	state_machine.physics_process(delta)

func flip_h(flipped):
	sprite.flip_h = flipped
