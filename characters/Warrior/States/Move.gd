extends Node

export var gravity = Vector2(0, 500.0)
var is_moveable = true
var jump_force = Vector2.ZERO
var max_speed = 0
var velocity = Vector2.ZERO
var acceleration = Vector2.ZERO

func get_direction():
	return Vector2(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"), 1.0)

func calculate_velocity(old_velocity, direction, acceleration, gravity, delta):
	var new_velocity = old_velocity + (gravity * delta)
	if is_moveable:
		new_velocity += direction * (acceleration * delta)
	new_velocity += jump_force

	return new_velocity

func physics_process(delta):
	var new_velocity = calculate_velocity(velocity, get_direction(), acceleration, gravity, delta)
	new_velocity.x = clamp(new_velocity.x, -max_speed, max_speed)
	
	var direction = get_direction()
	if direction.x > 0:
		owner.flip_h(false)
	elif direction.x < 0:
		owner.flip_h(true)
	
	if new_velocity.y > 0 and !owner.is_on_floor():
		owner.transition_state("Move/Fall")
	
	velocity = owner.move_and_slide(new_velocity, Vector2.UP)
	owner.emit_signal("moved", owner.global_position)

func is_on_floor():
	return owner.is_on_floor()
