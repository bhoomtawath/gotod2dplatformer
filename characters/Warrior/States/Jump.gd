extends Node

var move_max_speed = 300
# Horizontal acceleration, while jumping
var acceleration = Vector2(1000, 0)
var jump_force = Vector2(0, -200)

onready var parent = get_parent()

func on_entered():
	owner.animation.play("JUMP")
	parent.jump_force = jump_force

func physics_process(delta):
	parent.max_speed = move_max_speed
	parent.acceleration = acceleration
	
	if owner.is_on_floor():
		owner.transition_state("Move/Idle")

	parent.physics_process(delta)
	parent.jump_force = Vector2.ZERO
