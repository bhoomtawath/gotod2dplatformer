extends Node

var run_acceleration = Vector2(300, 0)
var run_max_speed = 300
var old_direction

onready var parent = get_parent()
onready var tween = $Tween

func on_entered():
	owner.animation.play("RUN")

func _jump():
	if Input.get_action_strength("jump") > 0:
		owner.transition_state("Move/Jump")

func _slow_down():
	tween.stop_all()
	tween.interpolate_property(parent, "max_speed", parent.max_speed, 0, 0.1, Tween.TRANS_EXPO, Tween.EASE_OUT)
	tween.start()

func _is_redirection(new_direction):
	if old_direction == null:
		return false
	
	return (old_direction.x > 0 and new_direction.x < 0) or (old_direction.x < 0 and new_direction.x > 0)

func physics_process(delta):
	var direction = parent.get_direction()
	parent.max_speed = run_max_speed
	if parent.is_on_floor():
		if direction.x != 0:
			if _is_redirection(direction):
				_slow_down()
			parent.acceleration = run_acceleration				
			_jump()
		else:
			owner.transition_state("Move/Idle")
		
		if Input.is_action_just_pressed("attack"):
			owner.transition_state("Move/Attack1")
	old_direction = direction
	parent.physics_process(delta)
