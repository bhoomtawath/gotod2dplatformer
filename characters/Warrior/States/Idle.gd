extends Node

onready var parent = get_parent()
onready var tween = $Tween
export var slow_duration_seconds = 0.5

func _slow_down():
	tween.interpolate_property(
		parent,
		"max_speed",
		parent.max_speed,
		0,
		slow_duration_seconds,
		Tween.TRANS_EXPO, 
		Tween.EASE_OUT
	)
	tween.start()

func on_entered():
	owner.animation.play("IDLE")

func physics_process(delta):
	var direction = parent.get_direction()
	if parent.is_on_floor():
		if direction.x != 0:
			owner.transition_state("Move/Run")
		if Input.get_action_strength("jump") > 0:
			owner.transition_state("Move/Jump")
		else:
			_slow_down()
			
		if Input.is_action_just_pressed("attack"):			
			owner.transition_state("Move/Attack1")
	
	parent.physics_process(delta)
