extends Node

onready var parent = get_parent()
onready var timer = $AttackDuration
onready var tween = $Tween
var hitbox

var damage = 50
var tween_duration = 0.2

export var chainable = false

func _ready():
	timer.connect("timeout", self, "_on_Attack1_timeout")
	hitbox = owner.get_node("HitBoxes/Attack1HitBox")

func _animate():
	owner.animation.play("ATTACK")

func _on_Attack1_timeout():
	parent.is_moveable = true
	owner.transition_state("Move/Idle")

func on_entered():
#	tween.interpolate_property(parent, "max_speed", parent.max_speed, 0, tween_duration, Tween.TRANS_EXPO, Tween.EASE_OUT)
#	tween.start()
	parent.max_speed = 0
	parent.is_moveable = false
	timer.start()
	_animate()

func physics_process(delta):
	if Input.is_action_just_pressed("attack") and chainable:
		timer.stop()
		chainable = false
		owner.transition_state("Move/Attack2")
		
	parent.physics_process(delta)

func _on_HitBox_body_shape_entered(body_id, body, body_shape, area_shape):
	var bodies = hitbox.get_overlapping_bodies()
	print(body_id)


func _on_Attack1HitBox_body_entered(body):
	if body.has_method("on_hitted"):
		body.on_hitted(damage)
		owner.emit_signal("attack_hitted", damage, body)
