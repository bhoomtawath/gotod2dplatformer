extends Node2D

onready var player = $Player
onready var fencing_partner = $FencingPartner
onready var label = $UI/Label
onready var ui_timer = $UI/Timer

func _ready():
	player.connect("attack_hitted", fencing_partner, "on_hitted")
	player.connect("moved", fencing_partner, "_on_Player_moved")
#	fencing_partner.connect("hitted", self, "_on_FencingPartner_hitted")
	label.visible = false
	
func _on_FencingPartner_hitted():
	label.text = "hitted"
	label.visible = true
	ui_timer.start()

func _on_Timer_timeout():
	label.visible = false
